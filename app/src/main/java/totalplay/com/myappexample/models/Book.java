package totalplay.com.myappexample.models;

import io.realm.RealmObject;

public class Book extends RealmObject {

    public String title;
    public String author;
    public float score;


}