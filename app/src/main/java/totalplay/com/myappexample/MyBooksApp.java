package totalplay.com.myappexample;

import android.app.Application;

import java.security.SecureRandom;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MyBooksApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        byte[] key = new byte[64];
        new SecureRandom(key);
        Realm.init(this);
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .encryptionKey(key).build();

        Realm.setDefaultConfiguration(realmConfiguration);

    }
}