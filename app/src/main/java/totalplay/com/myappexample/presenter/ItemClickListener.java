package totalplay.com.myappexample.presenter;

import totalplay.com.myappexample.models.Book;

public interface ItemClickListener {

    void onItemClick(Book book);

}