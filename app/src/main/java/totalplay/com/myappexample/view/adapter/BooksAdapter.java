package totalplay.com.myappexample.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.realm.RealmResults;
import totalplay.com.myappexample.R;
import totalplay.com.myappexample.models.Book;
import totalplay.com.myappexample.presenter.ItemClickListener;
import totalplay.com.myappexample.view.holders.ViewHolder;

public class BooksAdapter extends RecyclerView.Adapter<ViewHolder> {

    private RealmResults<Book> dataSet;
    private ItemClickListener itemClickListener;

    public BooksAdapter(RealmResults<Book> dataSet, ItemClickListener itemClickListener) {
        this.dataSet = dataSet;
        this.itemClickListener = itemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Book book = dataSet.get(position);
        holder.titleTextView.setText(book.title);
        holder.authorTextView.setText(book.author);
        holder.scoreTextView.setText(String.format("%s", book.score));

        holder.setItemClick(book, itemClickListener);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }


}