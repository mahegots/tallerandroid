package totalplay.com.myappexample.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import totalplay.com.myappexample.R;
import totalplay.com.myappexample.models.Book;
import totalplay.com.myappexample.presenter.ItemClickListener;
import totalplay.com.myappexample.view.adapter.BooksAdapter;

public class MainActivity extends AppCompatActivity implements ItemClickListener {

    public Realm realm;
    @BindView(R.id.booksRecyclerView)
    RecyclerView booksRecyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        realm = Realm.getDefaultInstance();
        booksRecyclerView.setHasFixedSize(true);
        booksRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateInfo();
    }

    @OnClick(R.id.act_main_new_book)
    public void createBook(View view) {
        Intent intent = new Intent(this, AddBookActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }


    public void updateInfo() {
        RealmQuery<Book> query = realm.where(Book.class);
        RealmResults<Book> results = query.findAll();
        results = results.sort("score", Sort.DESCENDING);
        booksRecyclerView.setAdapter(new BooksAdapter(results, this));
    }

    @Override
    public void onItemClick(Book book) {
        realm.beginTransaction();
        book.deleteFromRealm();
        realm.commitTransaction();
        updateInfo();
    }
}