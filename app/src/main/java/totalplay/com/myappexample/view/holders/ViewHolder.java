package totalplay.com.myappexample.view.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import totalplay.com.myappexample.R;
import totalplay.com.myappexample.models.Book;
import totalplay.com.myappexample.presenter.ItemClickListener;

public class ViewHolder extends RecyclerView.ViewHolder {

    public TextView titleTextView;
    public TextView authorTextView;
    public TextView scoreTextView;

    public ViewHolder(View itemView) {
        super(itemView);

        titleTextView = itemView.findViewById(R.id.tituloTextView);
        authorTextView = itemView.findViewById(R.id.autorTextView);
        scoreTextView = itemView.findViewById(R.id.puntajeTextView);
    }

    public void setItemClick(final Book book, final ItemClickListener itemClickOyente) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemClickOyente.onItemClick(book);
            }
        });
    }
}