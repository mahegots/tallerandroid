package totalplay.com.myappexample.view.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import totalplay.com.myappexample.R;
import totalplay.com.myappexample.models.Book;

public class AddBookActivity extends AppCompatActivity {

    public Realm realm;
    @BindView(R.id.act_added_title_text)
    EditText mTitleEditText;
    @BindView(R.id.act_added_author_text)
    EditText mAuthorEditText;
    @BindView(R.id.act_added_rating_bar)
    RatingBar mRatingBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_added);
        ButterKnife.bind(this);

        realm = Realm.getDefaultInstance();


    }

    public void saveBook(View view) {

        String title = mTitleEditText.getText().toString();
        String author = mAuthorEditText.getText().toString();
        float score = mRatingBar.getRating();

        realm.beginTransaction();
        Book li = realm.createObject(Book.class);
        li.title = title;
        li.author = author;
        li.score = score;
        realm.commitTransaction();

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realm.close();
    }
}